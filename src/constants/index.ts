export const PATTERN = '**** **** **** ****';

export const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

export const EXPIRATION_CARD_REJECTED = 'Expiration Date Exceeded';
export const CREDIT_CARD_REJECTED = 'Unsupported Card Number';
export const CREDIT_CARD_RESOLVED = 'Credit Card Saved';

// prettier-ignore
export const LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce finibus, nisl eget aliquet euismod, turpis velit dictum sem, sit amet venenatis elit nibh vel nisl."
