import { useState, ChangeEvent } from 'react';

export default function useField(initialState: string): [string, (e: ChangeEvent<HTMLInputElement>) => void] {
  const [value, setValue] = useState(initialState);
  return [value, e => setValue(e.target.value)];
}
