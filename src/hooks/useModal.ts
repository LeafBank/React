import { useState } from 'react';

interface UseModal {
  title: string;
  setTitle: (value: string) => void;
  isVisible: boolean;
  visibilityOn: () => void;
  visibilityOff: () => void;
  error: boolean;
  setError: (value: boolean) => void;
}

export default function useModal(): UseModal {
  const [title, setTitle] = useState('');
  const [visibility, setVisibility] = useState(false);
  const [error, setError] = useState(false);

  return {
    title,
    setTitle,
    isVisible: visibility,
    visibilityOn: () => setVisibility(true),
    visibilityOff: () => setVisibility(false),
    error,
    setError
  };
}
