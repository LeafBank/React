import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import useField from '../useField';

describe('useField', () => {
  it('Default', () => {
    const Field = () => {
      const [val, setVal] = useField('Hello World');

      return (
        <div>
          <label htmlFor="field">{val}</label>
          <input id="field" onChange={setVal} />
        </div>
      );
    };

    const { container, queryByText } = render(<Field />);

    const inputElement = container.querySelector('#field');

    expect(queryByText('Hello World')).toBeInTheDocument();
    expect(queryByText('Lorem Ipsum')).not.toBeInTheDocument();

    fireEvent.change(inputElement, { target: { value: 'Lorem Ipsum' } });

    expect(queryByText('Hello World')).not.toBeInTheDocument();
    expect(queryByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
