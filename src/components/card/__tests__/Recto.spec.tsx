import React from 'react';
import { render, screen } from '@testing-library/react';
import Recto from '../Recto';

describe('<Recto />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(
      <Recto cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34">
        <span>Hello World</span>
      </Recto>
    );

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(
      <Recto cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34">
        <span>Hello World</span>
      </Recto>
    );

    expect(screen.getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(screen.getByText('JOHN DOE')).toBeInTheDocument();
    expect(screen.getByText('12/34')).toBeInTheDocument();
    expect(screen.getByText('Hello World')).toBeInTheDocument();
  });
});
