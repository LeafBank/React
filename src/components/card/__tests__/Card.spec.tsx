import React from 'react';
import { render, screen } from '@testing-library/react';
import Card from '../Card';

describe('<Card />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(
      <Card reversed cardNumber="4321 1234 8765 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    );

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(<Card reversed cardNumber="4321 1234 8765 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />);

    expect(screen.getAllByText('**** 1234 **** 5678')).toHaveLength(2);
    expect(screen.getAllByText('JOHN DOE')).toHaveLength(2);
    expect(screen.getAllByText('12/34')).toHaveLength(2);
    expect(screen.getByText('567')).toBeInTheDocument();
  });
});
