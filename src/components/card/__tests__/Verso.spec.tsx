import React from 'react';
import { render, screen } from '@testing-library/react';
import Verso from '../Verso';

describe('<Verso />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(
      <Verso cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />
    );

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(<Verso cardNumber="**** 1234 **** 5678" fullName="JOHN DOE" month="12" year="34" crypto="567" />);

    expect(screen.getByText('**** 1234 **** 5678')).toBeInTheDocument();
    expect(screen.getByText('JOHN DOE')).toBeInTheDocument();
    expect(screen.getByText('12/34')).toBeInTheDocument();
    expect(screen.getByText('567')).toBeInTheDocument();
  });
});
