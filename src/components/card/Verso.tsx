import React from 'react';
import './verso.css';

type Props = {
  cardNumber: string;
  fullName: string;
  month: string;
  year: string;
  crypto: string;
};

function Verso(props: Props) {
  return (
    <div className="verso">
      <div className="half">
        <p className="leaf-bank row">
          <span className="title">LEAF</span>
          <span className="subtitle">bank</span>
        </p>
        <div className="magnetic-tape" />
      </div>

      <div className="half">
        <p className="card-number">{props.cardNumber}</p>
        <p className="crypto">{props.crypto}</p>

        <div className="corners">
          <p className="card-corner">
            <span className="value truncate">{props.fullName || 'Unknown'}</span>
          </p>
          <p className="card-corner">
            <span className="value">
              {props.month}/{props.year}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Verso;
