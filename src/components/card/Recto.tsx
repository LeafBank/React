import React, { ReactNode } from 'react';
import { Chip, NearFieldCom } from '../icons';
import './recto.css';

type Props = {
  cardNumber: string;
  fullName: string;
  month: string;
  year: string;
  children: ReactNode;
};

function Recto(props: Props) {
  return (
    <div className="recto">
      <div className="half">
        <div style={{ display: 'flex', padding: '0.5rem' }}>
          <div className="big-leaf">
            <div className="medium-leaf">
              <div className="small-leaf" />
            </div>
          </div>

          <p className="leaf-bank col">
            <span className="title">LEAF</span>
            <span className="subtitle">bank</span>
          </p>

          {props.children}
        </div>

        <div className="icons">
          <Chip style={{ marginLeft: '0.5rem' }} />
          <NearFieldCom style={{ marginLeft: '0.5rem' }} />
        </div>
      </div>

      <div className="half">
        <p className="card-number">{props.cardNumber}</p>
        <div className="corners">
          <p className="card-corner">
            <span className="key">fullname</span>
            <span className="value truncate">{props.fullName || 'Unknown'}</span>
          </p>

          <p className="card-corner">
            <span className="key">expires</span>
            <span className="value">
              {props.month}/{props.year}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Recto;
