import React from 'react';
import { LOREM_IPSUM } from '../constants';

type Props = {
  isVisible: boolean;
  isError?: boolean;
  title: string;
};

function Modal(props: Props) {
  return (
    <div className="modal-support" style={{ opacity: props.isVisible ? 1 : 0 }}>
      <div
        className="modal"
        style={{
          transform: props.isVisible ? 'scale(1)' : 'scale(0)'
        }}>
        <div
          className="modal-icon"
          style={{
            color: props.isError ? 'var(--red-500)' : 'var(--green-500)',
            backgroundColor: props.isError ? 'var(--red-300)' : 'var(--green-300)'
          }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={24}
            height={24}
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round">
            <rect x="1" y="4" width="22" height="16" rx="2" ry="2" />
            <line x1="1" y1="10" x2="23" y2="10" />
          </svg>
        </div>
        <p className="modal-title">{props.title}</p>
        <p className="modal-description">{LOREM_IPSUM}</p>
      </div>
    </div>
  );
}

export default Modal;
