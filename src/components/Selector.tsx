import React, { useState, ReactNode, MouseEvent } from 'react';

type Props = {
  defaultValue?: string;
  onClick?: (e: MouseEvent<HTMLButtonElement>) => void;
  ariaLabelledBy?: string;
  children: ({ onClose }: { onClose: (value: string) => void }) => ReactNode;
};

function Selector({ defaultValue = 'Unknown', onClick = () => {}, ...props }: Props) {
  const [value, setValue] = useState('');
  const [displayList, setDisplayList] = useState(false);

  const onClose = (value: string) => {
    setValue(value);
    setDisplayList(false);
  };

  return (
    <div style={{ position: 'relative' }}>
      <button
        className="selector"
        type="button"
        onClick={e => {
          onClick(e);
          setDisplayList(!displayList);
        }}
        aria-haspopup="listbox"
        aria-expanded={displayList}
        aria-labelledby={props.ariaLabelledBy}>
        {value || defaultValue}
        <svg width={16} height={16} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M8 9l4-4 4 4m0 6l-4 4-4-4"
            stroke="currentColor"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </button>

      {displayList && (
        <ul className="listbox" tabIndex={-1} role="listbox">
          {props.children({ onClose })}
        </ul>
      )}
    </div>
  );
}

export default Selector;
