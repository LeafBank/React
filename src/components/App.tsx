import React, { useEffect, useState, FormEvent } from 'react';
import { Card } from './card';
import Field from './Field';
import Modal from './Modal';
import Selector from './Selector';
import useField from '../hooks/useField';
import useModal from '../hooks/useModal';
import * as CreditCardService from '../services/creditCardService';
import { getYears, convertMonthToInt, convertYearToInt } from '../utils';
import { MONTHS } from '../constants';

function App() {
  const [reversed, setReversed] = useState(false);
  const [cardHolder, setCardHolder] = useField('');
  const [cardNumber, setCardNumber] = useField('');
  const [month, setMonth] = useState('');
  const [year, setYear] = useState('');
  const [crypto, setCrypto] = useField('');
  const [loading, setLoading] = useState(false);
  const modal = useModal();

  useEffect(() => {
    // eslint-disable-next-line
    console.log('Leaf Bank');
  }, []);

  // TODO: Move To Utils
  const isValid = (): boolean => {
    if (cardHolder.length === 0) {
      return false;
    }

    if (cardNumber.length === 0) {
      return false;
    }

    if (month.length === 0) {
      return false;
    }

    if (year.length === 0) {
      return false;
    }

    if (crypto.length === 0) {
      return false;
    }

    return true;
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setLoading(true);

    const expirationDate = new Date();
    expirationDate.setMonth(+month);
    expirationDate.setFullYear(+year);

    CreditCardService.validateCreditCard({
      cardHolder,
      cardNumber: cardNumber.replace(/\s/g, ''), // NOTE: Matches Whitespaces
      expirationDate,
      crypto
    })
      .then(res => {
        modal.setTitle(res.message);

        setTimeout(() => {
          modal.visibilityOn();
        }, 0.5 * 1000);
      })
      .catch(err => {
        modal.setError(true);
        modal.setTitle(err.message);

        setTimeout(() => {
          modal.visibilityOn();
        }, 0.5 * 1000);
      })
      .finally(() => setLoading(false));
  };

  return (
    <div>
      {modal.title && <Modal isVisible={modal.isVisible} title={modal.title} isError={modal.error} />}

      <Card
        reversed={reversed}
        cardNumber={cardNumber}
        fullName={cardHolder}
        month={convertMonthToInt(month)}
        year={convertYearToInt(year)}
        crypto={crypto}
      />

      <form onSubmit={handleSubmit}>
        <Field
          classNames="row"
          style={{ marginTop: 'auto' }}
          id="cardHolder"
          label="Card Holder"
          placeholder="John Doe"
          onChange={setCardHolder}
          onFocus={() => setReversed(false)}
        />

        <Field
          classNames="row"
          id="cardNumber"
          label="Card Number"
          placeholder="5678 1234 5678 1234"
          maxLength={19}
          onChange={e => {
            e.target.value = e.target.value
              .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
              .replace(/(.{4})/g, '$1 ') // NOTE: Any Chars, 4 Times
              .trim();
            setCardNumber(e);
          }}
          onFocus={() => setReversed(false)}
        />

        <div className="field-group">
          <div className="field" style={{ width: 100 }}>
            <label htmlFor="expiresThru">Expires Thru</label>
            <Selector defaultValue="Month" onClick={() => setReversed(false)} ariaLabelledBy="expiresThru">
              {({ onClose }) =>
                MONTHS.map((val, idx) => (
                  <li
                    key={idx}
                    id={`listbox-option-${idx}`}
                    className={`option${val === month ? ' selected' : ''}`}
                    role="option"
                    aria-selected={val === month}
                    onClick={() => {
                      setMonth(val);
                      setReversed(false);
                      onClose(val);
                    }}>
                    {val}
                  </li>
                ))
              }
            </Selector>
          </div>

          <div className="field" style={{ width: 100 }}>
            <Selector defaultValue="Year" onClick={() => setReversed(false)}>
              {({ onClose }) =>
                getYears(5, 20).map((val, idx) => (
                  <li
                    key={idx}
                    id={`listbox-option-${idx}`}
                    className={`option${val === year ? ' selected' : ''}`}
                    role="option"
                    aria-selected={val === year}
                    onClick={() => {
                      setYear(val);
                      setReversed(false);
                      onClose(val);
                    }}>
                    {val}
                  </li>
                ))
              }
            </Selector>
          </div>

          <Field
            style={{ width: 50 }}
            id="crypto"
            label="CVV"
            placeholder="567"
            maxLength={3}
            onChange={e => {
              e.target.value = e.target.value
                .replace(/[^\dA-Z]/g, '') // NOTE: Escapes Digits, Matches Words
                .trim();
              setCrypto(e);
            }}
            onFocus={() => setReversed(true)}
          />
        </div>

        <button className="submit" disabled={loading || !isValid()}>
          {loading ? 'Loading' : 'Submit'}
        </button>
      </form>
    </div>
  );
}

export default App;
