import React, { CSSProperties, ChangeEvent, FocusEvent } from 'react';

type Props = {
  classNames?: string;
  style?: CSSProperties;
  id: string;
  name?: string;
  label?: string;
  type?: string;
  placeholder?: string;
  maxLength?: number;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (e: FocusEvent<HTMLInputElement>) => void;
};

function Field({ style = {}, type = 'text', onChange = () => {}, onFocus = () => {}, ...props }: Props) {
  return (
    <div className={`field ${props.classNames}`} style={style}>
      {props.label && <label htmlFor={props.id}>{props.label}</label>}
      <input
        id={props.id}
        name={props.name || props.id}
        type={type}
        placeholder={props.placeholder}
        maxLength={props.maxLength}
        onChange={onChange}
        onFocus={onFocus}
      />
    </div>
  );
}

export default Field;
