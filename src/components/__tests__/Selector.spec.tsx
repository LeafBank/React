import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Selector from '../Selector';

describe('<Selector />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(<Selector>{() => <span>Hello World</span>}</Selector>);

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(
      <Selector defaultValue="Hello World" ariaLabelledBy="helloWorld">
        {() => <span>Lorem Ipsum</span>}
      </Selector>
    );

    expect(screen.queryByText('Lorem Ipsum')).not.toBeInTheDocument();
  });

  it('Should Emit Events', () => {
    const onClickMock = jest.fn();

    render(
      <Selector defaultValue="Hello World" onClick={onClickMock} ariaLabelledBy="helloWorld">
        {() => <span>Lorem Ipsum</span>}
      </Selector>
    );

    const buttonElement = screen.getByText('Hello World');

    userEvent.click(buttonElement);

    expect(screen.queryByText('Lorem Ipsum')).toBeInTheDocument();
    expect(onClickMock).toHaveBeenCalled();
  });
});
