import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Field from '../Field';

describe('<Field />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(<Field id="field" />);

    expect(container).toMatchSnapshot();
  });

  it('Should Renders', () => {
    render(
      <Field
        classNames="row"
        style={{ margin: 'auto' }}
        id="field"
        name="field"
        label="Field"
        type="text"
        placeholder="Field"
        maxLength={10}
      />
    );

    expect(screen.queryByText('Field')).toBeInTheDocument();
    expect(screen.queryByPlaceholderText('Field')).toBeInTheDocument();
  });

  it('Should Emit Events', () => {
    const onChangeMock = jest.fn();
    const onFocusMock = jest.fn();

    render(
      <Field
        classNames="row"
        style={{ margin: 'auto' }}
        id="field"
        name="field"
        label="Field"
        type="text"
        placeholder="Field"
        maxLength={10}
        onChange={onChangeMock}
        onFocus={onFocusMock}
      />
    );

    const fieldElement = screen.getByPlaceholderText('Field');

    fireEvent.focus(fieldElement);
    fireEvent.change(fieldElement, { target: { value: 'Hello World' } });

    expect(screen.queryByPlaceholderText('Field')).toHaveValue('Hello World');
    expect(onChangeMock).toHaveBeenCalled();
    expect(onFocusMock).toHaveBeenCalled();
  });
});
