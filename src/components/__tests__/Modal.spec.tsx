import React from 'react';
import { render } from '@testing-library/react';
import Modal from '../Modal';

describe('<Modal />', () => {
  it('Should Match The Snapshot', () => {
    const { container } = render(<Modal isVisible title="Hello World" />);

    expect(container).toMatchSnapshot();
  });

  it('Should Renders As Resolved', () => {
    const { container, getByText } = render(<Modal isVisible title="Hello World" />);

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(16, 185, 129);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });

  it('Should Renders As Rejected', () => {
    const { container, getByText } = render(<Modal isVisible isError title="Hello World" />);

    const iconElement = container.querySelector('.modal-icon');

    expect(iconElement).toHaveStyle({ color: 'rgb(239, 68, 68);' });
    expect(getByText('Hello World')).toBeInTheDocument();
    expect(getByText(/Lorem ipsum dolor sit amet/)).toBeInTheDocument();
  });
});
