import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../App';

describe('<App />', () => {
  const now = new Date();

  it('Should Match The Snapshot', () => {
    const { container } = render(<App />);
    expect(container).toMatchSnapshot();
  });

  it('Should Submit The Form With Invalid Date', async () => {
    const { getByPlaceholderText, getByText, queryByText } = render(<App />);

    const cardHolderElement = getByPlaceholderText('John Doe');
    fireEvent.change(cardHolderElement, { target: { value: 'Doe John' } });

    const cardNumberElement = getByPlaceholderText('5678 1234 5678 1234');
    fireEvent.change(cardNumberElement, {
      target: { value: '4321 1234 8765 5678' }
    });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() - 5}`);
    userEvent.click(yearOption);

    const cryptoElement = getByPlaceholderText('567');
    fireEvent.change(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Expiration Date Exceeded')).toBeInTheDocument();
    });
  });

  it('Should Submit The Form With Invalid Number', async () => {
    const { getByPlaceholderText, getByText, queryByText } = render(<App />);

    const cardHolderElement = getByPlaceholderText('John Doe');
    fireEvent.change(cardHolderElement, { target: { value: 'Doe John' } });

    const cardNumberElement = getByPlaceholderText('5678 1234 5678 1234');
    fireEvent.change(cardNumberElement, {
      target: { value: '1234 4321 5678 8765' }
    });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() + 5}`);
    userEvent.click(yearOption);

    const cryptoElement = getByPlaceholderText('567');
    fireEvent.change(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Unsupported Card Number')).toBeInTheDocument();
    });
  });

  it('Should Submit The Form', async () => {
    const { getByPlaceholderText, getByText, queryByText } = render(<App />);

    const cardHolderElement = getByPlaceholderText('John Doe');
    fireEvent.change(cardHolderElement, { target: { value: 'Doe John' } });

    const cardNumberElement = getByPlaceholderText('5678 1234 5678 1234');
    fireEvent.change(cardNumberElement, {
      target: { value: '4321 1234 8765 5678' }
    });

    const monthButton = getByText('Month');
    userEvent.click(monthButton);

    const monthOption = getByText('December');
    userEvent.click(monthOption);

    const yearButton = getByText('Year');
    userEvent.click(yearButton);

    const yearOption = getByText(`${now.getFullYear() + 5}`);
    userEvent.click(yearOption);

    const cryptoElement = getByPlaceholderText('567');
    fireEvent.change(cryptoElement, { target: { value: '123' } });

    const submitButton = getByText('Submit');
    userEvent.click(submitButton);

    await waitFor(() => {
      expect(queryByText('Credit Card Saved')).toBeInTheDocument();
    });
  });
});
